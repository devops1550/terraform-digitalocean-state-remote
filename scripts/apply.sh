# LOAD ENVIROMENT VARIABLES

if [ ! -f .env ]
then
  export $(cat "${OLDPWD}/.env" | xargs)
fi

# EXECUTE TERRAFORM APPLY

cd $OLDPWD && terraform apply